var ligModel = require('./ligModel.js');
var moment = require('moment');

/**
 * ligController.js
 *
 * @description :: Server-side logic for managing ligs.
 */
module.exports = {

    /**
     * ligController.list()
     */
    list: function (req, res) {
        ligModel.find({is_expired: false, ends_on: { $gt: moment().unix()}}).populate({path: 'locks'}).exec(function (err, ligs) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig.',
                    error: err
                });
            }
            return res.json(ligs); 
        });
    },

    get_expired: function (req, res) {
        ligModel.find({is_expired: true, ends_on: { $gt: moment().subtract(4, 'hour').unix()}}).populate({path: 'locks'}).exec(function (err, ligs) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig.',
                    error: err
                });
            }
            return res.json(ligs); 
        });
    },

    /**
     * ligController.show() 
     */
    show: function (req, res) {
        var ref = req.params.ref;
        ligModel.findOne({ref: ref}).populate('locks').exec(function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig.',
                    error: err
                });
            }
            if (!lig) {
                return res.status(404).json({
                    message: 'No such lig'
                });
            }
            return res.json(lig);
        });
    },
    /**
     * ligController.show() 
     */
    get_lig: function (req, res) {
        var ref = req.params.ref;
        ligModel.findOne({ref: ref}).populate({path: 'lock_logs', populate:{path: 'user'}}).exec(function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig.',
                    error: err
                });
            }
            if (!lig) {
                return res.status(404).json({
                    message: 'No such lig'
                });
            }
            return res.json(lig);
        });
    }, 

    /**
     * ligController.create()
     */
    create: function (req, res) {
        var lig = new ligModel({
            name : req.body.name,
            pricing: req.body.pricing.split(","),
			expiry : req.body.expiry * 60, //in minutes
			is_expired : false,
			//locks : req.body.locks,
            //starts_on : req.body.starts_on,
            config: {stopTime: moment().add(req.body.expiry, 'minute'), clock: ['d', 100, 2, 'h', 100, 2, 'm', 60, 2, 's', 60, 2, 'u', 10, 1], notify: [9]},
			ends_on : moment().add(req.body.expiry, 'minute'),
        });

        lig.save(function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating lig',
                    error: err
                });
            }
            return res.status(201).json(lig);
        });
    },

    expire_lig: function(req, res){
        var id = req.body.id;
        ligModel.findOne({_id: id}, function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig',
                    error: err
                });
            }
            if (!lig) {
                return res.status(404).json({
                    message: 'No such lig'
                });
            }

            lig.is_expired = true;
			
            lig.save(function (err, lig) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating lig.',
                        error: err
                    });
                }

                return res.json(lig);
            });
        });
    },

    /**
     * ligController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        ligModel.findOne({_id: id}, function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lig',
                    error: err
                });
            }
            if (!lig) {
                return res.status(404).json({
                    message: 'No such lig'
                });
            }

            lig.name = req.body.name ? req.body.name : lig.name;
			lig.ref = req.body.ref ? req.body.ref : lig.ref;
			lig.locks = req.body.locks ? req.body.locks : lig.locks;
			lig.starts_on = req.body.starts_on ? req.body.starts_on : lig.starts_on;
			lig.ends_on = req.body.ends_on ? req.body.ends_on : lig.ends_on;
			
            lig.save(function (err, lig) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating lig.',
                        error: err
                    });
                }

                return res.json(lig);
            });
        });
    },

    /**
     * ligController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        ligModel.findByIdAndRemove(id, function (err, lig) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the lig.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
