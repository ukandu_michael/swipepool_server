var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var paymentSchema = new Schema({
	'amount' : Number,
	'status' : Boolean,
	'user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'gateway' : String,
	'reference' : String,
	'moves' : {type: Number, default: 0},
	'paid_on' : Date
});

module.exports = mongoose.model('payment', paymentSchema);
