var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var withdrawSchema = new Schema({
	'user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'account_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'amount' : Number,
	'status' : Number,
	'date' : Date
});

module.exports = mongoose.model('withdraw', withdrawSchema);
