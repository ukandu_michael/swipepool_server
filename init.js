var createError = require('http-errors')
var express = require('express');
var path = require('path');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var config = require('./config');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');


var indexRouter = require('./routes/index');
var ligRouter = require('./lig/ligRoutes');
var lockRouter = require('./lock/lockRoutes');
var paymentRouter = require('./payment/paymentRoutes');
var withdrawRouter = require('./withdraw/withdrawRoutes');
var userRouter = require('./user/userRoutes');
var bankAccount = require('./bank_account/bank_accountRoutes');
 
var app = express();

mongoose.connect(config.MONGO_URI, {
  useNewUrlParser: true 
});
mongoose.connection.on('error', function(err) {
  console.log('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.err);
});

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/lig', ligRouter);
app.use('/api/lock', lockRouter);
app.use('/api/payment', paymentRouter);
app.use('/api/withdraw', withdrawRouter);
app.use('/api/user', userRouter);
app.use('/api/bank-account', bankAccount);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

require('./tasks/endswipe');

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
