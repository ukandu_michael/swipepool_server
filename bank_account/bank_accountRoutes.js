var express = require('express');
var router = express.Router();
var bank_accountController = require('./bank_accountController.js');
var authMiddleware = require('../middleware');
const { check, body } = require('express-validator');
var userModel = require('./../user/userModel');

/* 
 * GET
 */
router.get('/', authMiddleware.ensureAuthenticated, bank_accountController.list);

/*
 * GET
 */
router.get('/:id', bank_accountController.show);

/*
 * POST
 */
router.post('/', [authMiddleware.ensureAuthenticated, [
        body('amount').custom((value, {req})=>{
            return userModel.findOne({_id: req.user}).then(user=>{
                  if(user.balance < value){
                    return Promise.reject('Oops! You do not have sufficient balance');
                  }
            });

        }),
     // check('name').isEmpty()
]], bank_accountController.create);

/*
 * PUT
 */
router.put('/:id', bank_accountController.update);

/*
 * DELETE
 */
router.delete('/:id', bank_accountController.remove);

module.exports = router;
