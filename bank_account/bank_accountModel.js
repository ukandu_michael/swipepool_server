var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var bank_accountSchema = new Schema({
	'name' : String,
	//'bvn' : Number,
	'bank' : String,
	'account_number' : Number,
	'account_ref' : String,
	'user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	}
});

module.exports = mongoose.model('bank_account', bank_accountSchema);
